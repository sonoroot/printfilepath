package main

import (
	"fmt"
	"log"
	"os"
)

func main() {

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	if len(os.Args) > 1 {
		fmt.Println(dir + "/" + os.Args[1])
	} else {
		fmt.Println(dir)

	}
}
